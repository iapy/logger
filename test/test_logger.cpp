#include <logger/log.hpp>
#include <logger/logger.hpp>

#include <msgbus/msgbus.hpp>
#include <msgbus/workers.hpp>
#include <msgbus/service.hpp>
#include <tester/tester.hpp>

#include <cg/component.hpp>
#include <cg/service.hpp>

BOOST_AUTO_TEST_SUITE(Logger)

struct Test : cg::Component
{
    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                LOG_INFO("Message");
                LOG_DEBUG("Debug");
                std::string value = "test";
                std::this_thread::sleep_for(std::chrono::milliseconds(13));
                LOG_INFO("Message", (foo, 1)(bar, value));
                LOG_INFO("Message", (value));

                nlohmann::json foo;
                foo["a"] = 1;
                foo["b"] = 2;
                LOG_INFO("Message", (foo));

                return 0;
            }
        };
    };

    struct Worker
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            auto handler()
            {
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        std::chrono::seconds{2},
                        this->bind(&Interface<Base>::stop)
                    }
                };
            }
        private:
            void stop(std::unique_lock<std::mutex> &lock)
            {
                kill(getpid(), SIGUSR1);
            }
        };
    };

    template<typename>
    using State = msgbus::State<Worker>;
    using Services = ct::join_t<ct::tuple<Service>, msgbus::Workers<Worker>>;
};

using G = cg::Graph<msgbus::Clients<logger::Logger, Test>>;

BOOST_AUTO_TEST_CASE(TestLogger)
{
    std::ostringstream ss;
    std::clog.rdbuf(ss.rdbuf());

    BOOST_TEST(0 == msgbus::Service<G>());

    std::string const data = ss.str();
    BOOST_TEST(std::string::npos == data.find(R"(Debug)"));
    BOOST_TEST(std::string::npos != data.find(R"(Message)" "\n"));
    BOOST_TEST(std::string::npos != data.find(R"(Message -- {"bar":"test","foo":1})" "\n"));
    BOOST_TEST(std::string::npos != data.find(R"(Message -- {"value":"test"})" "\n"));
    BOOST_TEST(std::string::npos != data.find(R"(Message -- {"a":1,"b":2})" "\n"));
}

BOOST_AUTO_TEST_SUITE_END()
