#include <logger/log.hpp>

namespace logger::detail {
namespace {

void dummy(tag::Logger, char const *, int, char const *, std::optional<std::string> const &) {}

} // namespace

std::function<void(tag::Logger, char const *, int, char const *, std::optional<std::string> const &)> JsonMessage::emit = &dummy;

} // logger::detail
