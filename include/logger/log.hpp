#pragma once
#include <jsonio/serialize.hpp>
#include <boost/preprocessor.hpp>
#include <iostream>

namespace logger::tag {

enum class Logger : uint16_t
{
    DEBUG = 0,
    INFO,
    WARN,
    ERROR,
    FATAL
};

} // namespace logger::tag

namespace logger::detail {

struct JsonMessage
{
    inline JsonMessage() = default;

    template<typename T, typename ...Args>
    inline JsonMessage(const char *name, T const &value, Args&&... args)
    : JsonMessage(std::forward<Args>(args)...)
    {
        jsonio::to(json[name], value);
    }

    inline operator std::optional<std::string> () const
    {
        return json.is_null() ? std::nullopt : std::optional{json.dump()};
    }

    static std::function<void(tag::Logger, char const *, int, char const *, std::optional<std::string> const &)> emit; 
private:
    nlohmann::json json;
};

template<typename T>
auto check_arguments(T const &) -> std::is_same<T, nlohmann::json>;
auto check_arguments(...) -> std::false_type;

inline std::string dump(nlohmann::json const &json) { return json.dump(); }
std::string dump(...);

} // namespace logger::detail

#define LOG_DETAIL_MESSAGE_APPEND_1(a) #a, a
#define LOG_DETAIL_MESSAGE_APPEND_2(a, b) #a, b

#define LOG_DETAIL_MESSAGE_APPEND_N(...) BOOST_PP_OVERLOAD(LOG_DETAIL_MESSAGE_APPEND_, __VA_ARGS__)(__VA_ARGS__)
#define LOG_DETAIL_MESSAGE_APPEND(r, data, i, elem) BOOST_PP_COMMA_IF(i) LOG_DETAIL_MESSAGE_APPEND_N elem

#define LOG_DETAIL_CHECK_APPEND_1(a) a
#define LOG_DETAIL_CHECK_APPEND_2(a, b) #a, b

#define LOG_DETAIL_CHECK_APPEND_N(...) BOOST_PP_OVERLOAD(LOG_DETAIL_CHECK_APPEND_, __VA_ARGS__)(__VA_ARGS__)
#define LOG_DETAIL_CHECK_APPEND(r, data, i, elem) BOOST_PP_COMMA_IF(i) LOG_DETAIL_CHECK_APPEND_N elem

#define LOG_DETAIL_MESSAGE logger::detail::JsonMessage::emit

#define LOG_DETAIL_ADD_PAREN_1(...) ((__VA_ARGS__)) LOG_DETAIL_ADD_PAREN_2
#define LOG_DETAIL_ADD_PAREN_2(...) ((__VA_ARGS__)) LOG_DETAIL_ADD_PAREN_1
#define LOG_DETAIL_ADD_PAREN_1_END
#define LOG_DETAIL_ADD_PAREN_2_END
#define LOG_DETAIL_MAKE_PP_TUPLE_SEQ(seq) BOOST_PP_CAT(LOG_DETAIL_ADD_PAREN_1 seq, _END)

#define LOG_DETAIL_MAKE_MESSAGE(...) \
    logger::detail::JsonMessage{BOOST_PP_SEQ_FOR_EACH_I(LOG_DETAIL_MESSAGE_APPEND, _, LOG_DETAIL_MAKE_PP_TUPLE_SEQ(__VA_ARGS__))}

#define LOG_DETAIL_JSON_DUMP(...) \
    logger::detail::dump(BOOST_PP_SEQ_FOR_EACH_I(LOG_DETAIL_CHECK_APPEND, _, LOG_DETAIL_MAKE_PP_TUPLE_SEQ(__VA_ARGS__)))

#define LOG_DETAIL_CHECK_ARGUMENTS(...) \
    if constexpr(!decltype(logger::detail::check_arguments(BOOST_PP_SEQ_FOR_EACH_I(LOG_DETAIL_CHECK_APPEND, _, LOG_DETAIL_MAKE_PP_TUPLE_SEQ(__VA_ARGS__))))::value)

#ifndef DEBUG
#define LOG_DEBUG(...)
#else
#define LOG_DEBUG(message,...) LOG_DETAIL_CHECK_ARGUMENTS(__VA_ARGS__) { LOG_DETAIL_MESSAGE(logger::tag::Logger::DEBUG, __FILE__, __LINE__, message, LOG_DETAIL_MAKE_MESSAGE(__VA_ARGS__)); } else { LOG_DETAIL_MESSAGE(logger::tag::Logger::INFO, __FILE__, __LINE__, message, LOG_DETAIL_JSON_DUMP(__VA_ARGS__)); }
#endif

#define LOG_INFO(message,...)  LOG_DETAIL_CHECK_ARGUMENTS(__VA_ARGS__) { LOG_DETAIL_MESSAGE(logger::tag::Logger::INFO,  __FILE__, __LINE__, message,  LOG_DETAIL_MAKE_MESSAGE(__VA_ARGS__)); } else { LOG_DETAIL_MESSAGE(logger::tag::Logger::INFO, __FILE__, __LINE__, message, LOG_DETAIL_JSON_DUMP(__VA_ARGS__)); }
#define LOG_WARN(message,...)  LOG_DETAIL_CHECK_ARGUMENTS(__VA_ARGS__) { LOG_DETAIL_MESSAGE(logger::tag::Logger::WARN,  __FILE__, __LINE__, message,  LOG_DETAIL_MAKE_MESSAGE(__VA_ARGS__)); } else { LOG_DETAIL_MESSAGE(logger::tag::Logger::WARN, __FILE__, __LINE__, message, LOG_DETAIL_JSON_DUMP(__VA_ARGS__)); }
#define LOG_ERROR(message,...) LOG_DETAIL_CHECK_ARGUMENTS(__VA_ARGS__) { LOG_DETAIL_MESSAGE(logger::tag::Logger::ERROR, __FILE__, __LINE__, message, LOG_DETAIL_MAKE_MESSAGE(__VA_ARGS__)); } else { LOG_DETAIL_MESSAGE(logger::tag::Logger::ERROR, __FILE__, __LINE__, message, LOG_DETAIL_JSON_DUMP(__VA_ARGS__)); }
#define LOG_FATAL(message,...) LOG_DETAIL_CHECK_ARGUMENTS(__VA_ARGS__) { LOG_DETAIL_MESSAGE(logger::tag::Logger::FATAL, __FILE__, __LINE__, message, LOG_DETAIL_MAKE_KESSAGE(__VA_ARGS__)); } else { LOG_DETAIL_MESSAGE(logger::tag::Logger::FATAL, __FILE__, __LINE__, message, LOG_DETAIL_JSON_DUMP(__VA_ARGS__)); }

