#pragma once
#include <logger/log.hpp>
#include <cg/config.hpp>

#include <msgbus/state.hpp>
#include <msgbus/handler.hpp>
#include <msgbus/workers.hpp>

#include <cg/bind.hpp>
#include <cg/component.hpp>
#include <cg/connect.hpp>

#include <chrono>
#include <sstream>
#include <iostream>
#include <vector>
#include <optional>
#include <filesystem>

namespace logger {

struct Logger : cg::Component
{
    template<typename>
    struct Types
    {
        using Time = std::chrono::time_point<std::chrono::system_clock>;

        struct Message
        {
            Time time;
            tag::Logger level;
            char const *file;
            int line;
            std::string message;
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            auto handler()
            {
                using Self = Interface<Base>;
                using S = msgbus::State<Service>;
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        [this] { return !this->state.messages.empty(); },
                        this->bind(&Self::process)
                    }
                };
            }
        private:
            void process(std::unique_lock<std::mutex> &lock)
            {
                std::vector<typename Base::Message> messages;
                this->state.messages.swap(messages);
                lock.unlock();

                static constexpr char format[] = "%Y-%m-%d %H:%M:%S";
                static constexpr std::size_t count = sizeof("9999-12-31 23:59:29");
                char repr[count + sizeof(".000") - 1];

                for(auto &message : messages)
                {
                    std::time_t t = std::chrono::system_clock::to_time_t(message.time);
                    auto fine = std::chrono::time_point_cast<std::chrono::milliseconds>(message.time);

                    std::strftime(&repr[0], count, format, std::localtime(&t));
                    std::sprintf(&repr[count - 1], ".%03lld", fine.time_since_epoch().count() % 1000);

                    char const *f = message.file;
                    while(*message.file)
                    {
                        f = ++message.file;
                        while(*message.file && *message.file != '/')
                        {
                            ++message.file;
                        }
                    }
                    std::clog << '(' << repr << ") " << [level=message.level]() -> const char * const {
                        switch(level)
                        {
                            case tag::Logger::DEBUG: return "[DEBUG] ";
                            case tag::Logger::INFO:  return "[INFO]  ";
                            case tag::Logger::WARN:  return "[WARN]  ";
                            case tag::Logger::ERROR: return "[ERROR] ";
                            case tag::Logger::FATAL: return "[FATAL] ";
                            default: return "[WTF]   ";
                        }
                    }() << f << ':' << message.line << ' ' << message.message << '\n';
                }
                std::clog.flush();
            }
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            using S = msgbus::State<Service>;

            void message(tag::Logger level, char const *file, int line, char const *message, std::optional<std::string> const &json = std::nullopt)
            {
                if(static_cast<uint16_t>(level) < static_cast<uint16_t>(this->state.level))
                    return;

                std::ostringstream ss;
                ss << message;
                if(json)
                {
                    ss << " -- " << json.value();
                }
                auto m = typename Base::Message{
                    .time = std::chrono::round<std::chrono::milliseconds>(std::chrono::system_clock::now()),
                    .level = level,
                    .file = file,
                    .line = line,
                    .message = ss.str()
                };

                this->state.S::notify([&](){
                    this->state.messages.emplace_back(std::move(m));
                });
            }

            std::optional<std::string> rotate()
            {
                if(this->state.redirector)
                {
                    auto &redirector = *this->state.redirector;
                    auto path = std::remove_reference_t<decltype(redirector)>::rotated(redirector.path);
                    
                    this->state.lock([&]{
                        redirector.close();
                        std::filesystem::rename(redirector.path, path);
                        redirector.open(redirector.path);
                        std::clog.rdbuf(redirector.rdbuf());
                    });

                    return path;
                }
                else
                {
                    return std::nullopt;
                }
            }

            void stopped(std::function<void()> &&functor)
            {
                this->remote(msgbus::tag::Msgbus{}).stopped(std::forward<std::function<void()>>(functor));
            }
        };
    };

    struct Initializer
    {
        template<typename Base>
        struct Interface : Base
        {
            Interface(Base &&base) : Base{std::forward<Base>(base)}
            {
                logger::detail::JsonMessage::emit = [impl=this->local(tag::Logger{})](tag::Logger v, char const *f, int l, char const *m, std::optional<std::string> const &j) mutable {
                    impl.message(v, f, l, m, j);
                };
            }
        };
    };

    template<typename Types>
    struct State : msgbus::State<Service>
    {
        std::vector<typename Types::Message> messages;

        class Redirector : std::ofstream
        {
        public:
            Redirector(std::string const &path, std::ios_base::openmode const mode)
            : std::ofstream{path, mode}
            , path{path}
            , sbuf{std::clog.rdbuf(std::ofstream::rdbuf())}
            {}

            static std::string rotated(std::string const &p)
            {
                static constexpr char Format[] = "%Y%m%d_%H%M%S";
                static constexpr std::size_t count = sizeof("99991231_235929");

                std::string ext = std::filesystem::path{p}.extension();

                std::string path(p.size() + count, '.');
                std::strncpy(&path[0], p.c_str(), p.size() - ext.size());

                std::time_t t = std::chrono::system_clock::to_time_t(std::chrono::round<std::chrono::seconds>(std::chrono::system_clock::now()));
                std::strftime(&path[p.size() - ext.size() + 1], count, Format, std::localtime(&t));
                std::strncpy(&path[p.size() - ext.size() + count], ext.c_str(), ext.size());

                return path;
            }

            ~Redirector()
            {
                std::clog.rdbuf(sbuf);
            }
        private:
            friend class Impl;
            std::string const path;
            std::streambuf * const sbuf;
        };

        tag::Logger level{tag::Logger::DEBUG};
        std::unique_ptr<Redirector> redirector;

        State() = default;
        State(nlohmann::json const &json)
        {
            if(json.contains("path"))
            {
                std::string p = jsonio::from<std::string>(json["path"]);
                redirector = std::make_unique<Redirector>(p,
                        std::filesystem::exists(p)
                        ? std::ios_base::app | std::ios_base::out
                        : std::ios_base::out
                );
            }
            if(json.contains("level"))
            {
                std::string l = jsonio::from<std::string>(json["level"]);
                     if(l == "INFO")  level = tag::Logger::INFO;
                else if(l == "WARN")  level = tag::Logger::WARN;
                else if(l == "ERROR") level = tag::Logger::ERROR;
                else if(l == "FATAL") level = tag::Logger::FATAL;
            }
        }
    };

    using Ports = ct::map<ct::pair<tag::Logger, Impl>>;
    using Services = msgbus::Workers<Service>;
    using Initializers = ct::tuple<Initializer>;
};

} // namespace logger
