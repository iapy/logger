# Abstract

`logger` is a simplified asynchronous logging framework that utilises the [msgbus](https://bitbucket.org/iapy/msgbus) threading model for its functionality. All log entries are recorded in an independent thread dedicated to the log sink.

## Setting up

To configure logging for your application, you should incorporate the `logging::Logger`component into the graph and establish a connection with `msgbus::Msgbus`:

```c++
#include <logger/logger.hpp>
int main()
{
    using G = cg::Graph<
        Demo,
        msgbus::Clients<logger::Logger>
    >;
    return msgbus::Service<G>();
}
```

The `logger::Logger` is capable of being configured via JSON (refer to the [config](https://bitbucket.org/iapy/config) documentation for more information):

```json
{
    "logging": {
        "Logger": {
            "path": "/var/log/service.log"
        }
    }
}
```

## Using logger

```c++
#include <logger/log.hpp>

struct Demo
{
    struct Thread
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                LOG_DEBUG("Debug");
                LOG_DEBUG("Debug", (a, 1));

                std::string name{"test"};
                std::vector<int> ints{1,2,3};

                LOG_INFO("Info");
                LOG_INFO("Info", (a, 1)(name)(c, ints));

                auto json = "{\"foo\":\"bar\"}"_json;
                LOG_WARN("Warning");
                LOG_WARN("Warning", (json));

                LOG_ERROR("Error");
                LOG_ERROR("Error", (x, json));
                return 0;
            }
        };
    };
};

```

This will result in the following log output:

```
(2023-01-06 15:57:36.241) [INFO]  main.cpp:24 Info
(2023-01-06 15:57:36.241) [INFO]  main.cpp:25 Info -- {"a":1,"c":[1,2,3],"name":"test"}
(2023-01-06 15:57:36.241) [WARN]  main.cpp:28 Warning
(2023-01-06 15:57:36.241) [INFO]  main.cpp:29 Warning -- {"foo":"bar"}
(2023-01-06 15:57:36.241) [ERROR] main.cpp:31 Error
(2023-01-06 15:57:36.241) [ERROR] main.cpp:32 Error -- {"x":{"foo":"bar"}}
```

* `LOG_DEBUG` is ineffective unless `DEBUG` is defined, which typically happens in a debug build.
* The `LOG_` macro accepts two arguments: a mandatory message and an optional tuple that may contain variables and their respective names, which are also optional.

#### Logging format

The format for logging is set in advance and is not subject to modification:

```
(yyyy-mm-dd hh:MM:ss.mss) [LEVEL] file:line message -- json
```

* `mss`  stands for milliseconds
* An optional `json` tail, which follows `--`, is built from macro arguments according to these rules:
	* If an argument is an instance of `nlohmann::json`, it’s dumped.
	* if it is a `(variable)`, the returned json object will contain a field named `variable` (identical to the variable name in the `c++` code) with the value of that variable
	* If it is a `(name, expression)`, the resulting json object will contain a field named `name` with the value of the evaluated `expression`.

### Logger component

The `logging::Logger` exposes a single interface labeled `logging::tag::Logger` with the following methods:

```c++
template<typename Base>
struct Interface : Base
{
    template<tag::Logger Level> 
    void message(
        char const *file,
        int line,
        char const *message,
        std::optional<std::string> const &json = std::nullopt
    );

    std::optional<std::string> rotate();
};
```

- The `message` method is intended for those who wish to devise their own logging logic or incorporate `logging::Logger` with external libraries, as the weblib framework does.
- The `rotate` method rotates a log if it’s a file stream. It opens a new file at the `path` location and renames the current one by appending the current date and time (at the time of rotation) **before** the file extension. If the stream is a file, it returns the path to the newly created file; otherwise, it returns std::nullopt (i.e., when the logger logs to `std::clog`).
- Internally, `logging::Logger` employs `std::clog`, overriding its buffer when file logging is enabled. This implies that `std::clog` should not be directly used in application code to prevent interference with logging.
- All I/O operations are performed asynchronously in a separate thread.